<html>
<head>
    <title>HTML2PDF</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2 mt-5">
                <div class="card">
                    <div class="card-header bg-info">
                        <h1 class="text-center">HTML to PDF</h1>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('pdf.savepdf')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label><strong> Input HTML Code :</strong></label>
                                <textarea class="ckeditor form-control" name="description" cols="30" rows="20"></textarea>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info btn-sm float-right">Make PDF</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</body>
   
</html>


