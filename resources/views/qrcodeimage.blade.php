<html>
<head>
    <title>HTML2PDF</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2 mt-5">
                <div class="card">
                    <div class="card-header bg-info">
                        <h1 class="text-center">PDF to IMG</h1>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('pdf.saveimage')}}" enctype="multipart/form-data">
                            @csrf
                            <!-- <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate('http://trackagro.com.br')) }}" />

                            {!! QrCode::size(300)->generate('https://techvblogs.com/blog/generate-qr-code-laravel-8') !!} -->

                            <div class="form-group">
                                <label><strong> Upload a PDF file :</strong></label>
                                <input type="file" name="uploadpdf" class="btn btn-info">
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info btn-lg float-right">Convert IMG</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</body>
   
</html>


