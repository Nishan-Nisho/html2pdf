<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Imagick;

class PdfController extends Controller
{
    public function makepdf(){
        return view('editor');
    }

    public function savepdf(Request $request){
        
        $data = htmlspecialchars_decode($request->description);
        $pdf = \App::make('snappy.pdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->setPaper('a4')->setOrientation('portrait')->setOptions(['page-width'=>90, 'margin-top'=>10,'margin-bottom'=>10, 'margin-left'=>2,'margin-right'=>2]);
        return $pdf->stream();
    }

    public function makeimage(){
        return view('qrcodeimage');
    }
    public function saveimage(){
        // $snappy = \App::make('snappy.image');

        // $snappy = \App::make('snappy.image');
        // $html = view('welcome')->render();
        // $snappy->setOption('width',1); // FOR IMAGE WIDTH SET
        // $nameImage = \Str::random(5).'.jpg';
        // $snappy->generateFromHtml($html,public_path().$nameImage);

        //$path = 'images/Rusdid.jpg';
            // $path = 'G:\laragon\www\html2pdf\public\images\Rusdid.jpg';
            // dd($path);
            // $type = pathinfo($path, PATHINFO_EXTENSION);
            
            // $data = base64_encode(file_get_contents($path));
            // $data = readImage('G:\laragon\www\html2pdf\storage');
            // $json = json_decode(file_get_contents('http://...'));
            // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            // dd($base64);
            // return $base64;

            $im = new Imagick();
            $im->setResolution( 300, 300 );
            $im->readImage( "test.pdf" );


    }
}
