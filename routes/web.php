<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PdfController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('pdf')->group(function(){
    Route::get('/makepdf',[PdfController::class,'makepdf'])->name('pdf.makepdf');
    Route::post('/savepdf',[PdfController::class,'savepdf'])->name('pdf.savepdf');
    Route::get('/makeimage',[PdfController::class,'makeimage'])->name('pdf.makeimage');
    Route::get('/saveimage',[PdfController::class,'saveimage'])->name('pdf.saveimage');
});


